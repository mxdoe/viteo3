// vite.config.js
import { resolve } from 'path';
import { defineConfig } from 'vite';
import handlebars from 'vite-plugin-handlebars';
import react from '@vitejs/plugin-react';
// The .json that holds all the handblebars context variables
import handlebarsContext from './src/assets/json/handlebars-context.json';

export default defineConfig({
  plugins: [
    react(),
    handlebars({
// Imports handlebars context from a file. It's also possible to directly
// import but for the sake of a clean config file…
      context: () => {
          return handlebarsContext
      },
// Register custom handlebars helper here
      helpers: {
        list: function(context, options) {
                var ret = "<ul>";
                for (var i = 0, j = context.length; i < j; i++) {
                  ret = ret + "<li>" + options.fn(context[i]) + "</li>";
                }
                return ret + "</ul>";
        },
      },
// Adding the handlebars partials here.
// All .hbs files in a given directory get registered. But if you want to have
// a component based file structure, every component folder has to be registered
// here.
      partialDirectory: [resolve(__dirname, 'src/components/header'),
        resolve(__dirname, 'src/components/footer'),
        resolve(__dirname, 'src/components/nav'),
        resolve(__dirname, 'src/components/react'),
        resolve(__dirname, 'src/components/list'),
        resolve(__dirname, 'src/components/columns-three'),
      ],
    }),
  ],
  build: {
    rollupOptions: {
        input: {
        main: 'main.js',
// This here makes use of vites Multi-Page App capabilitys to create different
// views. During dev vite automatically creates the file structure inside of
// '/nested'. If you wanna use diffrent directorys of create multiple pages in
// production you have to specifiy these as entrypoints.
// In this way we are able to generate different views that can be used as
// frontend prototypes.
        index: resolve(__dirname, 'index.html'),
        react: resolve(__dirname, 'views/react.html'),
        noreact: resolve(__dirname, 'views/noreact.html'),
        documentation: resolve(__dirname, 'views/documentation.html'),
        faq: resolve(__dirname, 'views/faq.html'),
      },
    },
    sourcemap: true,
    manifest: true,
  },
// Removes console.log and debugger statements in production
  esbuild: {
      drop: ['console', 'debugger'],
  },
});
