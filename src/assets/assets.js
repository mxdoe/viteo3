// All assets that get imported AND listed in the 'assets' object are part of
// the assets object in manifest.json
import logo from '/favicon.svg'
import effLogo from '/src/assets/img/eff-logo.png'

const assets = {
  'List of assets that get included into manifest.json:': [
    logo,
    effLogo,
  ]
}
// this console.log is necessary! Otherwise Vite.js won't recognice the imports
// as used and will ignore them.
console.log(assets)
