import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';

function App (){
  
  const [counter, setCounter] = useState(0)

  const handleClick = () => {
    setCounter(counter + 1);
  }

  return (
    <>
      <h1>Wow, a Button!🎉</h1>
      <p>You already clicked it {counter} times</p>
      <button onClick={handleClick}>Click me!</button>
    </>
  )
}


export function init(){
  const components = document.querySelectorAll('.react')

  components.forEach(el => {
    const root = ReactDOM.createRoot(el);
    root.render(
      <App />
    );
    console.log('[loaded] react')
  })
}
