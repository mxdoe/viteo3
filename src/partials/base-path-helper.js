// This helper function selects all relative links on a page and adds the public
// base path variable
export function setBasePathInTemplates(){
    const links = document.querySelectorAll('a')
    
    for (let i=0; i < links.length; i++) {

      const href = links[i].attributes.href.nodeValue;

      if (href === "/"){
        links[i].setAttribute('href', import.meta.env.BASE_URL)
      } else if (checkForLocalPath(href)){
        links[i].setAttribute('href', import.meta.env.BASE_URL + href)
      }
    }

}

function checkForLocalPath(path) {
  return !/^(http)s?:\/\//.test(path);
}
