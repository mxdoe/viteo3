import './style.scss'
// This import is necessary to include assets into manifest.json
import './src/assets/assets.js'
import 'vite/modulepreload-polyfill'

const components = ['.footer','.header','.react','.nav']

async function init() {

  for (let i=0; i < components.length; i++){
    switch (components[i]){
      case ".header":
        if (document.querySelector(components[i])){
          console.log('header detected')
          const header = await import('./src/components/header/header.js');
          header.init()
        }
        break;
      case ".footer":
        if (document.querySelector(components[i])){
          console.log('footer detected')
          const footer = await import('./src/components/footer/footer.js');
          footer.init()
        }
        break;
      case ".react":
        if (document.querySelector(components[i])){
        console.log('react detected')
          const react = await import('./src/components/react/react.jsx');
          react.init()
        }
        break;
      case ".nav":
        if (document.querySelector(components[i])){
        console.log('nav detected')
        const nav = await import('./src/components/nav/nav.js');
        }
        break;
      default:
        break;
    }
  }

// If the public base path is set also set it this path in the all the html
// html templates
  if(import.meta.env.BASE_URL !== '/'){
    import('./src/partials/base-path-helper.js')
      .then(module => module.setBasePathInTemplates())
  }
}

init()
